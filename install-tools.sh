red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

echo "    ___ _       _______                 ";
echo "   /   | |     / / ___/                 ";
echo "  / /| | | /| / /\__ \                  ";
echo " / ___ | |/ |/ /___/ /                  ";
echo "/_/  |_|__/|__//____/                   ";
echo "    ____  _______    ______  ____  _____";
echo "   / __ \/ ____/ |  / / __ \/ __ \/ ___/";
echo "  / / / / __/  | | / / / / / /_/ /\__ \ ";
echo " / /_/ / /___  | |/ / /_/ / ____/___/ / ";
echo "/_____/_____/  |___/\____/_/    /____/  ";
echo "                                        ";
echo ""
echo " v1.0 by Luccas Brandão"
echo ""
echo "Script para Arch Linux"
echo ""
echo "${green}[AGUARDE] Instalando/atualizando o Git... ${reset}"

{
yes| pacman -S git
} &> /dev/null

echo "${green}[AGUARDE] Instalando/atualizando o NodeJS... ${reset}"

{
yes| pacman -S nodejs
} &> /dev/null

echo "${green}[AGUARDE] Instalando/atualizando o NPM... ${reset}"

{
yes| pacman -S npm
} &> /dev/null

echo "${green}[AGUARDE] Instalando/atualizando o Terraform... ${reset}"

{
yes| pacman -S terraform
} &> /dev/null

echo "${green}[AGUARDE] Instalando/atualizando o AWS CLI... ${reset}"

{
yes| pacman -S aws-cli
} &> /dev/null

echo "${green}Concluído!${reset}"
